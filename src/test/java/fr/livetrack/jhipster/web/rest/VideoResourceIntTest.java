package fr.livetrack.jhipster.web.rest;

import fr.livetrack.jhipster.LiveTrackApp;
import fr.livetrack.jhipster.domain.Video;
import fr.livetrack.jhipster.repository.VideoRepository;
import fr.livetrack.jhipster.repository.search.VideoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the VideoResource REST controller.
 *
 * @see VideoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LiveTrackApp.class)
@WebAppConfiguration
@IntegrationTest
public class VideoResourceIntTest {

    private static final String DEFAULT_COURSE_CATHEGORY = "AAAAA";
    private static final String UPDATED_COURSE_CATHEGORY = "BBBBB";
    private static final String DEFAULT_COURSE_NAME = "AAAAA";
    private static final String UPDATED_COURSE_NAME = "BBBBB";
    private static final String DEFAULT_COURSE_PATH = "AAAAA";
    private static final String UPDATED_COURSE_PATH = "BBBBB";
    private static final String DEFAULT_COURSE_DESCRIPTION = "AAAAA";
    private static final String UPDATED_COURSE_DESCRIPTION = "BBBBB";

    @Inject
    private VideoRepository videoRepository;

    @Inject
    private VideoSearchRepository videoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restVideoMockMvc;

    private Video video;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VideoResource videoResource = new VideoResource();
        ReflectionTestUtils.setField(videoResource, "videoSearchRepository", videoSearchRepository);
        ReflectionTestUtils.setField(videoResource, "videoRepository", videoRepository);
        this.restVideoMockMvc = MockMvcBuilders.standaloneSetup(videoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        videoSearchRepository.deleteAll();
        video = new Video();
        video.setCourseCathegory(DEFAULT_COURSE_CATHEGORY);
        video.setCourseName(DEFAULT_COURSE_NAME);
        video.setCoursePath(DEFAULT_COURSE_PATH);
        video.setCourseDescription(DEFAULT_COURSE_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createVideo() throws Exception {
        int databaseSizeBeforeCreate = videoRepository.findAll().size();

        // Create the Video

        restVideoMockMvc.perform(post("/api/videos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(video)))
                .andExpect(status().isCreated());

        // Validate the Video in the database
        List<Video> videos = videoRepository.findAll();
        assertThat(videos).hasSize(databaseSizeBeforeCreate + 1);
        Video testVideo = videos.get(videos.size() - 1);
        assertThat(testVideo.getCourseCathegory()).isEqualTo(DEFAULT_COURSE_CATHEGORY);
        assertThat(testVideo.getCourseName()).isEqualTo(DEFAULT_COURSE_NAME);
        assertThat(testVideo.getCoursePath()).isEqualTo(DEFAULT_COURSE_PATH);
        assertThat(testVideo.getCourseDescription()).isEqualTo(DEFAULT_COURSE_DESCRIPTION);

        // Validate the Video in ElasticSearch
        Video videoEs = videoSearchRepository.findOne(testVideo.getId());
        assertThat(videoEs).isEqualToComparingFieldByField(testVideo);
    }

    @Test
    @Transactional
    public void getAllVideos() throws Exception {
        // Initialize the database
        videoRepository.saveAndFlush(video);

        // Get all the videos
        restVideoMockMvc.perform(get("/api/videos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(video.getId().intValue())))
                .andExpect(jsonPath("$.[*].courseCathegory").value(hasItem(DEFAULT_COURSE_CATHEGORY.toString())))
                .andExpect(jsonPath("$.[*].courseName").value(hasItem(DEFAULT_COURSE_NAME.toString())))
                .andExpect(jsonPath("$.[*].coursePath").value(hasItem(DEFAULT_COURSE_PATH.toString())))
                .andExpect(jsonPath("$.[*].courseDescription").value(hasItem(DEFAULT_COURSE_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getVideo() throws Exception {
        // Initialize the database
        videoRepository.saveAndFlush(video);

        // Get the video
        restVideoMockMvc.perform(get("/api/videos/{id}", video.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(video.getId().intValue()))
            .andExpect(jsonPath("$.courseCathegory").value(DEFAULT_COURSE_CATHEGORY.toString()))
            .andExpect(jsonPath("$.courseName").value(DEFAULT_COURSE_NAME.toString()))
            .andExpect(jsonPath("$.coursePath").value(DEFAULT_COURSE_PATH.toString()))
            .andExpect(jsonPath("$.courseDescription").value(DEFAULT_COURSE_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVideo() throws Exception {
        // Get the video
        restVideoMockMvc.perform(get("/api/videos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVideo() throws Exception {
        // Initialize the database
        videoRepository.saveAndFlush(video);
        videoSearchRepository.save(video);
        int databaseSizeBeforeUpdate = videoRepository.findAll().size();

        // Update the video
        Video updatedVideo = new Video();
        updatedVideo.setId(video.getId());
        updatedVideo.setCourseCathegory(UPDATED_COURSE_CATHEGORY);
        updatedVideo.setCourseName(UPDATED_COURSE_NAME);
        updatedVideo.setCoursePath(UPDATED_COURSE_PATH);
        updatedVideo.setCourseDescription(UPDATED_COURSE_DESCRIPTION);

        restVideoMockMvc.perform(put("/api/videos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedVideo)))
                .andExpect(status().isOk());

        // Validate the Video in the database
        List<Video> videos = videoRepository.findAll();
        assertThat(videos).hasSize(databaseSizeBeforeUpdate);
        Video testVideo = videos.get(videos.size() - 1);
        assertThat(testVideo.getCourseCathegory()).isEqualTo(UPDATED_COURSE_CATHEGORY);
        assertThat(testVideo.getCourseName()).isEqualTo(UPDATED_COURSE_NAME);
        assertThat(testVideo.getCoursePath()).isEqualTo(UPDATED_COURSE_PATH);
        assertThat(testVideo.getCourseDescription()).isEqualTo(UPDATED_COURSE_DESCRIPTION);

        // Validate the Video in ElasticSearch
        Video videoEs = videoSearchRepository.findOne(testVideo.getId());
        assertThat(videoEs).isEqualToComparingFieldByField(testVideo);
    }

    @Test
    @Transactional
    public void deleteVideo() throws Exception {
        // Initialize the database
        videoRepository.saveAndFlush(video);
        videoSearchRepository.save(video);
        int databaseSizeBeforeDelete = videoRepository.findAll().size();

        // Get the video
        restVideoMockMvc.perform(delete("/api/videos/{id}", video.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean videoExistsInEs = videoSearchRepository.exists(video.getId());
        assertThat(videoExistsInEs).isFalse();

        // Validate the database is empty
        List<Video> videos = videoRepository.findAll();
        assertThat(videos).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchVideo() throws Exception {
        // Initialize the database
        videoRepository.saveAndFlush(video);
        videoSearchRepository.save(video);

        // Search the video
        restVideoMockMvc.perform(get("/api/_search/videos?query=id:" + video.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(video.getId().intValue())))
            .andExpect(jsonPath("$.[*].courseCathegory").value(hasItem(DEFAULT_COURSE_CATHEGORY.toString())))
            .andExpect(jsonPath("$.[*].courseName").value(hasItem(DEFAULT_COURSE_NAME.toString())))
            .andExpect(jsonPath("$.[*].coursePath").value(hasItem(DEFAULT_COURSE_PATH.toString())))
            .andExpect(jsonPath("$.[*].courseDescription").value(hasItem(DEFAULT_COURSE_DESCRIPTION.toString())));
    }
}
