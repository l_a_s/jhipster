/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package fr.livetrack.jhipster.web.rest.dto;
