(function() {
    'use strict';

    angular
        .module('liveTrackApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
