(function() {
    'use strict';

    angular
        .module('liveTrackApp')
        .controller('VideoDetailController', VideoDetailController);

    VideoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Video'];

    function VideoDetailController($scope, $rootScope, $stateParams, entity, Video) {
        var vm = this;
        vm.video = entity;
        vm.load = function (id) {
            Video.get({id: id}, function(result) {
                vm.video = result;
            });
        };
        var unsubscribe = $rootScope.$on('liveTrackApp:videoUpdate', function(event, result) {
            vm.video = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
