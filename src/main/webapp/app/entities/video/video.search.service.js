(function() {
    'use strict';

    angular
        .module('liveTrackApp')
        .factory('VideoSearch', VideoSearch);

    VideoSearch.$inject = ['$resource'];

    function VideoSearch($resource) {
        var resourceUrl =  'api/_search/videos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
