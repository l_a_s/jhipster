(function() {
    'use strict';

    angular
        .module('liveTrackApp')
        .controller('VideoDialogController', VideoDialogController);

    VideoDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Video'];

    function VideoDialogController ($scope, $stateParams, $uibModalInstance, entity, Video) {
        var vm = this;
        vm.video = entity;
        vm.load = function(id) {
            Video.get({id : id}, function(result) {
                vm.video = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('liveTrackApp:videoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.video.id !== null) {
                Video.update(vm.video, onSaveSuccess, onSaveError);
            } else {
                Video.save(vm.video, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
